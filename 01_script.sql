
CREATE EXTENSION pgcrypto;

create table users (
  usr_id serial PRIMARY KEY NOT NULL,
  usr_nombres text NOT NULL,
  usr_correo text NOT NULL UNIQUE,
  usr_clave text NOT NULL,
  created_at timestamp, 
  updated_at timestamp, 
  usr_estado text NOT NULL
);

insert into users ( usr_nombres, usr_correo, usr_clave, usr_estado ) values
('Roberto Morales E', 'rme.plan.b@gmail.com', crypt('123456', gen_salt('bf')), 'A');

create table posts (
  pst_id serial PRIMARY KEY NOT NULL,
  pst_usr_id integer NOT NULL, 
  pst_titulo text NOT NULL,
  pst_contenido text NOT NULL,
  pst_fecha timestamp NOT NULL DEFAULT NOW(),
  created_at timestamp, 
  updated_at timestamp, 
  pst_estado text NOT NULL,
  FOREIGN KEY(pst_usr_id) REFERENCES users(usr_id)
);

insert into posts ( pst_usr_id, pst_titulo, pst_contenido, pst_fecha, pst_estado ) values
(1, 'Primer Post', 'Primer post de prueba', '2023-02-08 08:30', 'A'),
(1, 'Segundo Post', 'Segundo post de prueba', '2023-02-08 10:01', 'A');

