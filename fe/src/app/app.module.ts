import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsuarioDeleteComponent } from './usr/usuario-delete/usuario-delete.component';
import { UsuarioEditComponent } from './usr/usuario-edit/usuario-edit.component';
import { UsuarioNewComponent } from './usr/usuario-new/usuario-new.component';
import { UsuariosListComponent } from './usr/usuarios-list/usuarios-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsuariosListComponent,
    UsuarioNewComponent,
    UsuarioEditComponent,
    UsuarioDeleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
