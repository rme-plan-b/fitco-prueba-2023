import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsuarioDeleteComponent } from './usr/usuario-delete/usuario-delete.component';
import { UsuarioEditComponent } from './usr/usuario-edit/usuario-edit.component';
import { UsuarioNewComponent } from './usr/usuario-new/usuario-new.component';
import { UsuariosListComponent } from './usr/usuarios-list/usuarios-list.component';

const routes: Routes = [
  { path: 'home', component: AppComponent },
  { path: 'login', component: LoginComponent },

  { path: 'usuariosList', component: UsuariosListComponent },
  { path: 'usuarioNew', component: UsuarioNewComponent },
  { path: 'usuarioEdit', component: UsuarioEditComponent },
  { path: 'usuarioDelete', component: UsuarioDeleteComponent },

  { path: '**', pathMatch: 'full', redirectTo: 'home' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
