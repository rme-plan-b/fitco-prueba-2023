import { Request, Response } from 'express';
import Usuario from '../models/usuario';

import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

export const getUsuarios = async (req: Request, res: Response) => {
    const usuarios = await Usuario.findAll();
    res.json({usuarios});
}

export const getUsuario = async (req: Request, res: Response) => {
    const { id } = req.params;
    const usuario = await Usuario.findByPk( id );

    if( usuario) {
        res.json({
            usuario
        });        
    } else {
        res.status(404).json({
            msg: `No existe usuario con ID ${ id };`
        });
    }
}

export const postUsuario = async (req: Request, res: Response) => {
    const { body } = req;

    try {
        const emailRepetido = await Usuario.findOne({
            where: {
                usr_correo: body.usr_correo
            }
        });

        if (emailRepetido) {
            return res.status(400).json({
                msg: 'Existe un usuario con este correo ' + body.usr_correo
            });
        }

        let pwd = bcrypt.hashSync(body.usr_clave, Number.parseInt(process.env.TOKEN_ROUNDS || '10') );
        body.usr_clave = pwd;

        Usuario.create( 
                body 
            ).then(user => {
                let token = jwt.sign({ _id: user.usr_id}, process.env.TOKEN_SECRET || 'tokenfitco', {
                    expiresIn: process.env.TOKEN_EXPIRES
                });
                res.json({
                    user,
                    token: token
                });
            });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error en postUsuario'
        })
    }
}

export const putUsuario = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { body } = req;

    try {
        const usuario = await Usuario.findByPk( id );
        if ( !usuario ) {
            return res.status(404).json({
                msg: 'No existe un usuario con ID ' + id
            })
        }

        await usuario.update( body );
        res.json({ usuario });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error en putUsuario'
        });
    }
}

export const deleteUsuario = async (req: Request, res: Response) => {
    const { id } = req.params;

    try {
        const usuario = await Usuario.findByPk( id );
        if ( !usuario ) {
            return res.status(404).json({
                msg: 'No existe un usuario con ID ' + id
            })
        }

        await usuario.update({ usr_estado: 'X'});
        res.json({ usuario });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error en deleteUsuario'
        });
    }
}

export const postLogin = async (req: Request, res: Response) => {
    const { usr_correo, usr_clave } = req.body;

        Usuario.findOne({
            where: { 
                usr_correo: usr_correo
            }
        }).then(user => {
            if ( !user ) {
                res.status(404).json({ msg: "Usuario no existe"})
            } else {
                let pwd = bcrypt.compareSync(usr_clave, user.usr_clave);

                console.log('xxxx', pwd);

                if ( pwd ) {
                    const token = jwt.sign({ _id: user.usr_id }, process.env.TOKEN_SECRET || 'tokenfitco', {
                        expiresIn: process.env.TOKEN_EXPIRES || '1d'
                    });
                    res.header({'api-token': token}).json({
                        user
                    });
                } else {
                    res.status(401).json({ msg: "Contraseña incorrecta"});
                }
            }
        }).catch(err => {
            res.status(500).json(err);
        })        

}
