"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const db = new sequelize_1.Sequelize('fitco', 'postgres', '123456y', {
    host: 'localhost',
    dialect: 'postgres',
    define: {
        underscored: true,
        createdAt: 'created_at',
        updatedAt: 'updated_at',
    },
    //logging: false,
});
exports.default = db;
//# sourceMappingURL=connection.js.map