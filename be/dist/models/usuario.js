"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const connection_1 = __importDefault(require("../db/connection"));
const Usuario = connection_1.default.define('users', {
    usr_id: {
        type: sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    usr_nombres: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
    },
    usr_correo: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: {
                msg: "El campo correo debe ser un correo valido"
            }
        }
    },
    usr_clave: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
        validate: {
            len: {
                args: [6, 255],
                msg: "minimamente tiene que tener 6 caracteres"
            }
        }
    },
    usr_estado: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false
    },
});
exports.default = Usuario;
//# sourceMappingURL=usuario.js.map