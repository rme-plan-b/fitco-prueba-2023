"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const usuarios_1 = require("../controllers/usuarios");
//import * as cors from 'cors';
const cors_1 = __importDefault(require("cors"));
const verifyToken_1 = require("../libs/verifyToken");
const router = (0, express_1.Router)();
const options = {
    allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
    ],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: '*',
    preflightContinue: false,
};
router.use((0, cors_1.default)(options));
router.options('*', (0, cors_1.default)(options));
router.get('', usuarios_1.getUsuarios);
router.get('/:id', verifyToken_1.TokenValidation, usuarios_1.getUsuario);
router.post('', verifyToken_1.TokenValidation, usuarios_1.postUsuario);
router.put('/:id', verifyToken_1.TokenValidation, usuarios_1.putUsuario);
router.delete('/:id', verifyToken_1.TokenValidation, usuarios_1.deleteUsuario);
router.post('/login', usuarios_1.postLogin);
exports.default = router;
//# sourceMappingURL=usuario.js.map