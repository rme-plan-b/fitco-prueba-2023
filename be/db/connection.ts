import { Sequelize } from 'sequelize';

const db = new Sequelize('fitco', 'postgres', '123456y', {
    host: 'localhost', 
    dialect: 'postgres',
    define: {
        underscored: true,  
        createdAt: 'created_at',
        updatedAt: 'updated_at',      
    },
    //logging: false,
});

export default db;
