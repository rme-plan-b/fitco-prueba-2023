import { Router } from 'express'
import { deleteUsuario, getUsuario, getUsuarios, postUsuario, putUsuario, postLogin } from '../controllers/usuarios';
//import * as cors from 'cors';
import cors from "cors";

import { TokenValidation } from '../libs/verifyToken';

const router = Router();

const options: cors.CorsOptions = {
    allowedHeaders: [
      'Origin',
      'X-Requested-With',
      'Content-Type',
      'Accept',
      'X-Access-Token',
    ],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: '*',
    preflightContinue: false,
};

router.use( cors( options ) );

router.options('*', cors(options));

router.get( '', getUsuarios );
router.get( '/:id', TokenValidation, getUsuario );
router.post( '', TokenValidation, postUsuario );
router.put( '/:id', TokenValidation, putUsuario );
router.delete( '/:id', TokenValidation, deleteUsuario );

router.post( '/login', postLogin );



export default router;