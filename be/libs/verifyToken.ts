import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export const TokenValidation = (req: Request, res: Response, next: NextFunction) => {
    const token = req.header('api-token');
    if ( !token ) {
        return res.status(401).json('Acceso denegado');
    }
    const payload = jwt.verify(token, process.env.TOKEN_SECRET || 'tokenfitco');
    //console.log('>>>>', payload);
    
    next();
}

