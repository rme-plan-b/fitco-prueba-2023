import { DataTypes } from "sequelize";
import db from '../db/connection';

const Usuario = db.define('users', {
    usr_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    usr_nombres: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    usr_correo: {
        type: DataTypes.TEXT,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: {
                msg: "El campo correo debe ser un correo valido"
            }
        }
    },
    usr_clave: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
            len: {
                args: [6, 255],
                msg: "minimamente tiene que tener 6 caracteres"
            }
        }
    },
    usr_estado: {
        type: DataTypes.TEXT,
        allowNull: false
    },
});


export default Usuario;