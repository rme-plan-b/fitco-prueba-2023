import express, { Application} from 'express'
import userRoutes from '../routes/usuario';

import { Request } from "express";
import cors from "cors";

import db from '../db/connection';

class Server {
    private app: Application;
    private port: string;
    private apiPaths = {
        usuarios: '/api/usuarios',
    }

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '3000';

        // base
        this.dbConnection();
        this.middlewares();
        this.routes();
    }

    async dbConnection() {
        try {
            await db.authenticate();
            console.log('BD conectada');
        } catch (error) {
            console.log(error);
            //throw new Error( error );
        }
    }

    middlewares() {
        // cors
	    const allowedOrigins = ['http://localhost:4200'];
        // const options: cors.CorsOptions = {
        //     origin: allowedOrigins
        // };
        const options: cors.CorsOptions = {
            allowedHeaders: [
              'Origin',
              'X-Requested-With',
              'Content-Type',
              'Accept',
              'X-Access-Token',
            ],
            credentials: true,
            methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
            origin: ['http://localhost:4200/'],
            preflightContinue: false,
        };        
        this.app.use( cors( options ) );

        // add body
        this.app.use( express.json() );

        // public
        this.app.use( express.static('public') );

    }


      
    routes() {
        this.app.use( this.apiPaths.usuarios, userRoutes );
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port);
        });
    }
}

export default Server;

